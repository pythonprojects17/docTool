import time
import os


def document_funcs(f):

	def foo(*args, **kwargs):
		start = time.time()
		rv = f(*args, **kwargs)
		end = time.time()
		temp = os.getcwd()
		os.chdir("C:\\Users\\magshimim\\Desktop")
		with open(f'{__name__}_doc.txt', 'a') as file:
			file.write(f"in file: {__file__}\nfunc: {f.__name__}\ninput: {args}\nkeyword argument: {kwargs}\noutput: {rv}\ntime: {end - start}\n\n")
		
		os.chdir(temp)
		return rv

	return foo
